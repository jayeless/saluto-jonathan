## Quaresma chapitro

Ube es Jonathan? Kad ilu stacas en Wien? No, ilu stacas en **diferanta** urbo. Ilu stacas en Bistriţa. Quo es Bistriţa? Ol es urbo en Rumania. Nun **komencas** la voyajo en Rumania.

Jonathan skribas en **sua** jurnalo: “La unesma mayo me esis en München. La duesma mayo me esis en Wien. **Pose** me esis en Budapest. Budapest esis bela. Ma me ne havis tempo en Budapest. Me vidis la **strado** en Budapest. En Budapest me pensis: hike **finas** la **westo**, e **hike** komencas la **esto**. Budapest es diferanta.”

Ilu pensa **poke** plu pri Budapest. Ilu pensa: “Me esas **persono** qua **venas** de Anglia, de London. Me **parolas** la angla, **nam** la personi en Anglia parolas la **angla**. Ma en Budapest me ne parolis kun la personi en la angla; me parolis en la **germana**. Mea **savo** di la germana ne esas bona, ma la personi en Budapest ne parolas la angla. **Ni** parolis en la germana. Es **interesanta** ke la personi en Budapest parolas la germana.”

Nun Jonathan pensas pri sua voyajo. Ilu pensa: “Yo esis en la **muzeo** en London. En la muzeo esis **libri** pri **altra** lando. Me **lektis** la libri. En la muzeo esis **mapi** pri altra **lando**. Me vidis la mapi. La libri e la mapi dicis ke la altra lando es **fora**, ma interesanta. En la altra lando esas multa personi, multa diferanta personi.”

Nun Jonathan pensas plu pri sua voyajo. Ilu pensas: “Hiere me esis en Klausenburg. La **nomo** dil **manjajo** esis ‘paprika hendl’. Ol es tre bona, la manjajo en Klausenberg. La **hotelo** esis bona, ma me ne **dormis** bone. Ma la lando es tre bela! En la treno me vidis multa **urbeti** e multa **kasteli**. Urbo es plu granda **kam** urbeto, ma la urbeti es tre bela. Kastelo es plu granda kam hotelo, e plu bela kam hotelo! Urbeti es **min** granda kam urbi, ma **li** es bela, e hoteli es min granda kam kasteli.”

Jonathan skribas plu en sua jurnalo: “Bistriţa es interesanta **loko**. Bistriţa es anciena loko, plu anciena kam altra loki. **Anke** mea hotelo en Bistriţa es anciena, plu anciena kam altra hoteli. En la hotelo me dicis mea nomo: ‘Me es Jonathan Harker’ e li dicis a me: ‘**Bonvenez** al hotelo.’ En la hotelo me vidis **letro** por me. Nun me lektos mea letro.”

Jonathan lektas sua letro. Olu dicas:

> Mea **amiko**. Bonvenez. Dorm**ez** bone. Me esas en Bukovina, bonvole venez a Bukovina. Ka la voyajo de London esis bona? Mea lando es tre bela. Bonvenez a mea lando.
>
> Vua amiko,
> DRACULA

Jonathan lektas la letro e pensas pri sua amiko **Komto** Dracula. **Qua** il es? Ilu ne savas. Ma ilua amiko es interesanta. Anke la urbeto Bistriţa es interesanta. Jonathan es felica, e pensas pri sua amiko. Pose ilu dormas.

### Gramatiko

#### plu kam, min kam

- plu kam: >
  - Jonathan es plu granda kam jurnalo. = Jonathan > jurnalo (Jonathan es granda, la jurnalo ne es granda)
  - Manjar bifsteko es plu bona kam ne manjar bifsteko. = Manjar un bifsteko > ne manjar bifsteko (manjar bifsteko es bona, ne manjar bifsteko ne es bona)
- min kam: <
  - Domo es min granda kam kastelo. = dom < kastelo (Kastelo es granda, domo ne es granda)
  - Manjar jurnalo es min bona kam manjar bifsteko. = manjar jurnalo < manjar bifsteko (manjar bifsteko es bona; manjar jurnalo ne es bona)

#### vua, sua, ilua

- vua
  - Jonathan manjas bifsteko. Persono dicas: “**Vu** manjas. Ka **vua** manjajo es bona?” (vua = di Jonathan)
  - Jonathan pensas pri Dracula. Ilu pensas: “Dracula, **vu** es bona viro. **Vua** lando es tre bela.”
- sua
  - Jonathan havas jurnalo quo es jurnalo di Jonathan. Jonathan havas **sua** jurnalo.
  - Jonathan manjas bifsteko. Jonathan manjas **sua** bifsteko.
- ilua
  - Jonathan amas la lando di Dracula. Jonathan amas **ilua** lando. (ilua = di Dracula)
  - Jonathan venos al kastelo di Dracula. Jonathan venos al **ilua** kastelo.

#### -ez

- Dracula dicas a Jonathan: “Venez a mea kastelo.”

#### qua

- _Qua_ es por personi.
  - Jonathan havas letro. **Qua** skribis la letro? Dracula es **qua** skribis la letro.
- _Quo(n)_ ne es por personi.
  - Jonathan vidis mapo. **Quon** Jonathan vidis? Mapo es **quon** Jonathan vidis.

#### altra

- Jonathan venas de Anglia. Ilu ne venas de Rumania. Rumania es **altra** lando. (= Rumania ne es Anglia, ol es **altra** lando.)

#### fora

- Jonathan esas en la treno hodie, il esis en la treno hiere, il esos en la treno morge. Rumania es **fora**.

### Vortaro

- altra
- amiko
- angla, Anglia
  - angl.a
  - Angl.ia
- anke
- bonvenez
  - bon.ven.ez
- diferanta: ≠
  - difer.ant.a
- dormas
- finas
- germana, Germania
  - german.a
  - German.ia
- hike
- hotelo
- ilua
- interesanta
  - interes.ant.a
- kam
- kastelo 🏰
- komencas
- komto
- lando
- lektas
- letro ✉️
- li
- libro 📖
- loko
- manjajo 🍱
- mapo 🗺
- min
- muzeo
- nam
- ni = me ed altro, od altri
- nomo
- ost
- parolas 💬
- persono 🧑🏽
- poke = ne multe
- pose
- qua
- savas, savo
  - sav.as
  - sav.o
- strado 
- sua
- urbeto
- venas
- vua
- westo