## Duesma Chapitro

### Duesma mayo (2 mayo)

Nun la viro esas en la treno. Ilu ne esas en München; il esas en treno. Ilu **voyajas**. Ilu pensas: “Nun me voyajas **de** München **a** Wien. Ol es bona **voyajo**. Me **amas** voyaj**i**.”

Ilu pensas pri München. Ilu pensas: “Nun me esas en la treno, ma **hiere** me es**is** en München. E nun me skribas jurnalo en la treno, **ma** hiere me skribis jurnalo en München. E nun me pensas en la treno, ma hiere me pensis en München. Hiere me pensis pri München en München, e nun me pensas pri Wien en la treno. Nun me esas en la treno, ne en Wien. Ma me pensas e skribas pri Wien.”

Ka la viro nun pensas en München? No, ilu nun ne pensas en München. Ilu pensas en la treno. Hiere ilu pensis en München. Ilu dicis: “Saluto, treno!”

La viro esas en la treno, ed ilu voyajas **ad** urbo. La urbo ne esas München; München esas la urbo **di** hiere. La urbo esas Wien; Wien esas la urbo di **hodie**.

La viro pensas pri München e Wien. Ilu pensas: “München esis la urbo di hiere, e München esis bona. Nun es hodie, e me esas en treno; la treno es bona. Ka Wien es**os** bona?”

La viro pensas pri München: München esis la urbo di hiere. Ilu pensas en la treno: ilu esas en la treno hodie. Ed ilu pensas pri Wien: Wien esos la urbo di **morge**.

Ed ilu pensas: “München esis granda. La treno esas granda. Ka Wien esos granda?”

Ed ilu pensas: “En München me skribis en jurnalo. En la treno me skribas en jurnalo. Ka en Wien me skribos en jurnalo? Yes, morge en Wien me skribos en jurnalo. Me amas jurnali.” La viro pensas **multe** (ilu pensas multe = ilu pensas e pensas e pensas), ed ilu skribas multe. Yes, il esas inteligenta viro. Inteligenta viri skribas multe, e pensas multe. Il es Jonathan; Jonathan esas inteligenta viro.

Ilu skribas: “Me esas Jonathan. Me esas en treno. Hiere me stacis en München; morge me stacos en Wien.”

Ilu pensas, e skribas: “La treno... ol es bona, ma **anciena**. Ol ne es **nova**; ol es anciena. Ka en München la treni es anciena? Yes, la treni di München es anciena. Ma la treni di München es bona, e me amas la treni di München. Hiere me amis la treno en München, e hodie me amas la treno nun, e morge me amos la treno en Wien. Me amas treni!”

Jonathan skribas: “München es bona urbo ed anciena urbo, e Wien es bona urbo ed anciena urbo. München e Wien ne es nova, ma es bona. München e Wien es anciena, ma bona urbi. La urbi ne es nova, ma bona. Me amas urbi!”

Jonathan pensas, ke la duesma dio **dil** voyajo es bona. Ilu dicas: “Hodie esis bona duesma **dio** di voyajo. Me amas voyaji!”

### Gramatiko

#### hiere, hodie, morge

- hiere = unesma mayo
- hodie = duesma mayo
- morge = triesma mayo

#### -is, -as, -os

|         | 1 mayo (hiere)   | 2 mayo (hodie)              | 3 mayo (morge)   |
| ------- | ---------------- | --------------------------- | ---------------- |
| skribar | Jonathan skribis | Jonathan skribas            | Jonathan skribos |
| pensar  | Jonathan pensis  | Jonathan pensas             | Jonathan pensos  |
| vidar   | Jonathan vidis   | Jonathan vidas              | Jonathan vidos   |
| esar    | Jonathan esis    | Jonathan esas / Jonathan es | Jonathan esos    |

#### de, a

- **De** München **a** Wien
  - München → Wien
- **De** Wien **a** München
  - München ← Wien

#### di

- la jurnalo **di** Jonathan = la jurnalo en quon Jonathan skribas
- la treno **di** Jonathan = la treno sur quo Jonathan es

#### a, de, di + la

- la treno **al** urbo = la treno **a la** urbo
- la treno **del** urbo = la treno **de la** urbo
- la treno **dil** urbo = la treno **di la** urbo

### Vortaro

- a = ad
- amas ❤️
- anciena
- de
- di
- dio
- hiere (**hiere** → hodie → morge)
- hodie (hiere → **hodie** → morge)
- morge (hiere → hodie → **morge**)
- multe
- nova
- voyajas
- voyajo

