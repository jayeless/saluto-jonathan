## Triesma Chapitro

### Duesma mayo (2 mayo)

Jonathan stacas en Wien; la urbo di Wien. Jonathan pensas ke Wien es bona, e ke Wien es **bela**. Jonathan ne pensas ke Wien es **desbela**; ilu pensas ke Wien es bela. Ilu dicas: “Wien es bela urbo! Me skribos pri **lo**!” Ed ilu skribas en la jurnalo pri Wien. En la jurnalo ilu skribas: “**Mea** triesma dio es **tre** bona! En la duesma dio me stacis en München. Ma me nun stacas en Wien: Wien ne es München. München e Wien es du urbi; München ne es Wien e Wien ne es München. Me stacas en Wien e la urbo es tre bela. Me amas Wien; la urbo ne es desbela. **Quante** bela es Wien! Ma me **havas** problemo.”

Quo? Jonathan havas **problemo**? Qua problemo? Nun ilu ne vidas la jurnalo; ilu vidas la urbo e pensas. Ilu pensas multe pri la problemo.

La problemo di Jonathan es ke il amas Wien, ma ilu ne havas **tempo**.

Ilu pensas: “Hmm. Nun esas sep (7) **kloki**. Ye dek (10) kloki la treno **departos**. Dek (10) kloki **minus** sep (7) kloki es tri (3) **hori**. Tri hori ne es multa tempo **por** bela urbo! Me ne havas tempo. Hav**ar** tempo es bona, ma me ne havas lo! Skribas jurnalo esas bona, ma me ne havas tempo por skribar lo! Quon **facar** en Wien?”

Ilu pensas: “Me ne havas multa tempo. Quon facar – skribar jurnalo, o **manjar**, o vidar la urbo? Quon me facos?”

Ilu pensas **plu**, e dicas: “Me havas bona **ideo**! **Vartez**… me pensas. Mea tren departas **ye** dek (10) kloki. Ka esas treno plu **tarda**, ye dek-e-un (11) kloki, **o** dek-e-du (12), dek-e-tri (13), dek-e-quar (14) o dek-e-kin (15) kloki?”

Ilu vidas... yes! Esas treno quo departas ye dek-e-quin kloki. Nun Jonathan es **felica**.

Jonathan dicas: “Dek-e-quin (15) minus sep (7) es ok (8). Nun me havas ok hori de tempo! Quon me facos?”

Ilu dicas: “Me savas! Me manjos **bifsteko**. E me drinkos **biro**. Vartez... no, me **drinkos** du biri, o tri biri. Bona ideo!”

Nun Jonathan drinkas un biro en Wien, e manjas bifsteko. Il es felica. Il dicas: “Quante me esas felica! Me amas la urbo di Wien. Quante me amas voyaji!”

### Gramatiko

#### Verbi

- La infinitivo di verbo es **-ar**. Pensas = pensar, skribas = skribar, manjas = manjar, havas = havar.
  - Jonathan pensas: “Quon manjar?” ed ilu manjas bifsteko.
  - Jonathan pensas: “Havar tempo es bona!” Il ne havas tempo.

#### plu tarde

- Esas 10 kloki. **Plu tarde**, esos 11 kloki.
- Jonathan esis en München. **Plu tarde**, il esis en Wien.
- Jonathan pensis: “Me manjos.” **Plu tarde**, ilu manjis.

#### mea

- **Me** skribas en **mea** jurnalo. (Mea jurnalo = la jurnalo di me)

#### quo, quon, qua

- Quo?
  - Treno esas anciena. **Quo** esas anciena? *La treno* esas anciena.
- Quon?
  - Jonathan vidas urbo. **Quon** Jonathan vidas? Jonathan vidas *la urbo*.
- Quo
  - La treno departos. Quo departos? La treno es **quo** departos.
  - La urbo es bela. Quo es bela? La urbo es **quo** es bela.
- Quon
  - Jonathan es en anciena treno. Jonathan amas la treno. La treno **quon** Jonathan amas es anciena.
  - Jonathan vidas Wien. Wien es urbo. La urbo **quon** Jonathan vidas es Wien.
- Qua?
  - En **qua urbo** Jonathan stacas nun?
  - **Qua problemo** esas?

#### hori, kloki

- kloko
  - Es sis kloki: 6:00
  - Es du kloki e dek: 2:10 
- horo = 60 minuti. Un horo = 60 minuti, du hori = 120 minuti.

6:00 es **sis kloki** e 2:00 es **du kloki**. 6:00 minus 2:00 es **quar hori**.

#### quante = !

- La treno es bela. **Quante** bela es la treno!
- Jonathan es inteligenta. **Quante** inteligenta il es!

#### vartez

- “Vartez!” = ne nun, plu tarde

#### un, du, tri...

- 1: un
- 2: du
- 3: tri
- 4: quar
- 5: kin
- 6: sis
- 7: sep
- 8: ok
- 9: non
- 10: dek
- 11: dek-e-un
- 12: dek-e-du

### Vortaro

- bela ↔ desbela
- bifsteko 🥩
- biro 🍺
- departas
- desbela ↔ bela
- drinkas
- facas
- felica 😁
- havas
- horo: 24 hori en un dio
- ideo 💡
- kloko
- lo
- manjas 
- mea
- minus
- o = od
- plu
- por
- problemo
- quante
- tarda
- tempo
- tre
- vartez
- ye