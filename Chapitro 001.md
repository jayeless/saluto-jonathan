## Unesma Chapitro

### Unesma mayo (1 mayo)

**Viro stacas en urbo. La viro skribas jurnalo. La viro vidas urbo.**

**Ka** la viro stacas en urbo? **Yes**, ilu stacas en urbo.

Ka la viro stacas en… viro? **No**, ilu **ne** stacas en viro. **Ilu** stacas en urbo.

Ka la viro stacas en **treno**? No, ilu ne stacas en treno. Ilu stacas en urbo. La viro stacas en urbo.

Ka la viro skribas jurnalo? Yes, ilu skribas jurnalo.

Ka la jurnalo skribas la viro? No, la jurnalo ne skribas la viro. Jurnalo ne skribas. Viro skribas. La viro skribas la jurnalo.

Ka la urbo vidas la viro? No, la urbo ne vidas la viro. Urbo ne vidas. Viro vidas. Ka la viro vidas la urbo? Yes, la viro vidas la urbo.

**Ube** stacas la viro? Ilu stacas en urbo. **Quon** la viro skribas? Ilu skribas jurnalo. Quon la viro vidas? Ilu vidas urbo. Ilu stacas en urbo, **ed** ilu skribas jurnalo, ed ilu vidas urbo.

Ka la viro *stacas* en jurnalo? No, ilu *skribas* en jurnalo. Ka la viro vidas viro? No, ilu ne vidas viro; ilu vidas urbo.

La viro es **granda**. La viro es **bona**, e la viro es **inteligenta**. Ilu pensas. Ilu pensas pri la urbo. Ilu pensa: “**Quo** es la urbo? Ka la urbo **es** bona? Ka la urbo es granda?”

Ka la viro skribas urbo? No, ilu ne skribas urbo; urbo es granda. Ilu skribas jurnalo; jurnalo ne es granda.

Ka la jurnalo pensas **pri** la viro? No, jurnalo ne pensas. Viro pensas. Ilu pensas pri la jurnalo, e pensas pri la urbo.

Ed ilu skribas en la jurnalo. Ilu skribas pri la urbo. Ilu skribas: “La urbo es bona, e la urbo es granda.”

Ilu pensas: “La urbo es bona”; ilu pensas **ke** la urbo es bona. Ilu pensas: “La urbo es granda”; ilu pensas ke la urbo es granda. Ilu pensas pri la urbo, ed ilu pensas pri la jurnalo.

Quo es la jurnalo? La jurnalo esas ube la viro skribas; ilu skribas en la jurnalo. En la jurnalo, la viro skribas pri la urbo. En la jurnalo, la viro ne skribas pri la **treno**; la viro stacas en la urbo, ne en la treno. Ilu ne pensas pri la treno; ilu pensas pri la urbo, la urbo München.

Quo es la urbo? **Ol** esas München. Ube es München? Ol es ube la viro stacas. Ube esas la viro? Il esas en München.

Yes, München es granda urbo, e bona urbo. La viro pensas ke München es bona urbo, ed ilu pensas ke ol es granda urbo. La viro es inteligenta. Ilu **dicas**: “**Saluto**, München!” Il es bona viro!

La viro stacas e pensas: “Ube esas la treno?” Ilu vidas... ilu vidas la treno! Ilu pensas: “La treno!” **Nun** ilu ne pensas pri la jurnalo e ne pensas pri la urbo; ilu pensas pri la treno!

### Gramatiko

#### ka / kad

- ka = kad = ?

#### -esm-

- un = 1
  - un.esm.a
- du = 2
  - du.esm.a
- tri = 3
  - tri.esm.a
- Mayo 2019 es 2019-05.
  - Unesma mayo esas 2019-05-01.
  - Duesma mayo esas 2019-05-02.
  - Triesma mayo esas 2019-05-03.

### Vortaro

- bona 👍
- dicas 🗣️
- e = ed
- en
- es = esas
- granda
- il = ilu
- inteligenta 🧠
- jurnalo 📝
- ka = kad
- ke
- la = l'
- ne esas ↔ esas
- no ↔ yes
- nun
- ol = olu
- pensas 💭
- pri
- quo = quon
- saluto 👋
- skribas 🖋️
- stacas 🧍
- treno 🚂
- ube 🗺️ 🤷‍♂️
- un = 1
- urbo 🏙️
- vidas 👀
- viro 👨🏻
- yes ↔ no