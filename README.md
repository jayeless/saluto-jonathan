# Saluto, Jonathan!

_Saluto, Jonathan!_ (Hello, Jonathan!) is the story of a young Englishman who goes to visit his friend in another European country. This edition is written entirely in Ido. It begins with very simple language. The words the reader knows are repeated and new words are added slowly and in context, so that no dictionary is required to read it. This is a method used also by other books such as *English by the Nature Method*, *Le français par la méthode nature*, and *L’italiano secondo il metodo natura*, as well as Hans Ørberg's *Lingua Latina per se Illustrata*.

The [original _Salute, Jonathan!_](https://en.wikibooks.org/wiki/Salute,_Jonathan!) is a book written in Occidental, and first published by Dave MacLeod on Wikibooks in 2019. The text is available under the [CC BY-SA 3.0 licence](https://creativecommons.org/licenses/by-sa/3.0/). As such, the text of this translation into Ido is also available under the same terms. It is also available [on Wikibooks](https://en.wikibooks.org/wiki/Saluto,_Jonathan!_(Ido)).

The original _Salute, Jonathan!_ consists of 100 chapters. As you can see, right at this moment this translation consists only of a small portion of it. Nonetheless, the further you get into the book, the language becomes more and more complex and an actual story begins to form. _Salute, Jonathan!_ is in fact a full translation of a classic book that you are likely very familiar with. Go read it, in Occidental, if you’d like to know more.

## Chapter List

1. [Unesma Chapitro](Chapitro 001.md)
2. [Duesma Chapitro](Chapitro 002.md)
3. [Triesma Chapitro](Chapitro 003.md)
4. [Quaresma Chapitro](Chapitro 004.md)
5. [Kinesma Chapitro](Chapitro 005.md)