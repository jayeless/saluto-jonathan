## Kinesma chapitro

### Quaresma mayo (4 mayo)

Hodie Jonathan voyajos a Komto Dracula. En la hotelo ilu vidas la **hotelisto**, e vidas letro de Komto Dracula en la **manuo** dil hotelisto. Ilu dicas al hotelisto en la germana: “Saluto, **bonvole** donez la letro a me.” La hotelisto **donas** la letro ad ilu.

Pose ilu **questionas** en la germana: “Quo es la letro? Quon Dracula skribis en la letro?” Ma la hotelisto dicas: “Me ne parolas la germana, me ne parolas la germana.” Jonathan pensas: “**Stranje**! Hiere me parolis kun la hotelisto en la germana, e nun ilu dicas ke ne parolas la germana! **Pro quo**?” Ilu questionas al hotelisto: “Ka vu savas qua es Komto Dracula?” ma la hotelisto ne parolas.

Jonathan pensas: “Stranje! Me parolis kun la hotelisto en la germana. Ilu **devas** parolar la germana, ma ilu dicas ke ilu ne parolas ol! Me devas questionar ilu plu, ma me ne havas tempo. Me devas departar.”

Nun Jonathan esas en sua **chambro** en la hotelo. Pos un horo, ilu devas departar. Ma **regardez**! **Yen** olda **muliero** qua venis ad ilua chambro.

La **olda** muliero: “Pro quo vu departas? Bonvole ne departez! Hodie ne es bona dio! Es **mala** dio, dio kun mala **kozi**!”

Jonathan: “Me esas bona, me **iras** por vidar amiko. Bonvole ne parolez pri mala kozi.”

La olda muliero: “Ka vu ne savas ke hodie es la dio di **Santo** Georgio? Ye dek-e-du (12) kloki la mala kozi venos! Ka vu savas ube vu iras?”

Jonathan: “No, no, olda muliero, **omno** esas bona. Me esas bona, e hodie ne venos mala kozi.”

La olda muliero: “**Me pregas** ke vu ne departez! Bonvole ne departez!”

Jonathan: “No, me devas departar. Bonvole ne **plorez**.”

La olda muliero: “Yen **krucifixo**. La krucifixo es por vu.”

Jonathan: “Ah... la **angliani** ne amas krucifixi... ma **danko**. Bonvole ne plorez.” Jonathan pensas: “En Anglia ni amas **kruci**, ma ne krucifixi. En Rumania la personi amas krucifixi, ne kruci.”

Nun la olda muliero iras e Jonathan esas **sola** en sua chambro. Ilu pensas: “**Ca** loko es stranja! Olda muliero qua donas krucifixi! Ma me devas departar.”

### Gramatiko

#### -ist-

- Viro en hotelo es hotel**ist**o. Jonathan iras al hotel e parolas al hotel**ist**o.

#### qui

- _Qui_ es por plu kam un persono o kozo.
  - **Qui** venas de Anglia? Angliani esas **qui** venas de Anglia.
  - **Quin** la olda muliero donas? Krucifixi esas **quin** la olda muliero donas.

#### devar

- devar + infinitivo di verbo
  - Jonathan ne havas tempo; ilu **devas** departar
  - Ma la hotelisto savas la germana; ilu **devas** parolar la germana!

#### bonvole, me pregas

- _bonvole_ es adverbo
  - Bonvole ne plorez.
  - Bonvole venez a mea kastelo.
- _me pregas_ havas _ke_ pose
  - Me pregas ke vu ne plorez.
  - Me pregas ke vu venez a mea kastelo.

#### omno

- Omno esas bona = ne esas mala kozo (esas dek kozi, e dek kozi esas bona)
- Omno esas mala = ne esas bona kozo (esas dek-e-kin kozi, e dek-e-kin kozi esas mala)
- Me savas omno = ne esas kozo quon me ne savas

### Vortaro

- angliani: personi qui venas de Anglia
  - angli.an.o
- bonvole = me pregas
- ca = ica
- chambro
- danko
- devar
- donar
- hotelisto
  - hotel.ist.o
- irar
- kozo
- kruco
- krucifixo
- mala = desbona
- manuo ✋
- me pregas = bonvole
- muliero 👩🏻
- olda 🧓🏼
- omno
- plorar 😭
- pro quo
- questionar ❓
- regardar 👀
- santo
- sola: ne esar kun altri
- stranja
- yen = hike esas